﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleWebApiCrud.Models
{
    public interface IDataLayer
    {
        List<Student> ReadAll();
        void Delete(Guid id);
    }

    public class DataLayer : IDataLayer
    {
        private readonly List<Student> _students; 
        public DataLayer()
        {
            _students = new List<Student>
            {
                new Student(Guid.NewGuid(), "Cameron", "Presley", Grade.Freshmen),
                new Student(Guid.NewGuid(), "Houston", "Miller", Grade.Sophomore),
                new Student(Guid.NewGuid(), "Travis", "Donnell", Grade.Junior),
                new Student(Guid.NewGuid(), "Stacy", "Mullinax", Grade.Senior)
            };
        }

        public List<Student> ReadAll()
        {
            return _students;
        } 

        public void Delete(Guid id)
        {
            var studentToDelete = _students.First(x => x.Id == id);
            _students.Remove(studentToDelete);
        }
    }
}